import { TestBed } from "@angular/core/testing";

import { RoutesGuardService } from "./routes.guard.service";

describe("RoutesGuardService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: RoutesGuardService = TestBed.get(RoutesGuardService);
    expect(service).toBeTruthy();
  });
});
