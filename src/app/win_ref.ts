import { Injectable, Inject, PLATFORM_ID } from "@angular/core";
import { isPlatformBrowser, isPlatformServer } from "@angular/common";

@Injectable({ providedIn: "root" })
export class WindowRef {
  constructor(@Inject(PLATFORM_ID) private platformId: Object) {}

  get nativeWindow(): any {
    if (isPlatformBrowser(this.platformId)) {
      return window;
    }
  }

  clearLogStorage(): void {
    if (isPlatformBrowser(this.platformId)) {
      if (localStorage.getItem("session_token")) {
        localStorage.removeItem("session_token");
      }
    }
  }

  getLocalSession(): string {
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.getItem("session_token");
    }
  }

  setLocalSession(token: string): void {
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.setItem("session_token", token);
    }
  }
  clearLocalSession(): void {
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.removeItem("session_token");
    }
  }

  getUrl(): string {
    if (isPlatformBrowser(this.platformId)) {
      return (
        this.nativeWindow.location.protocol +
        "//" +
        this.nativeWindow.location.hostname +
        ":" +
        this.nativeWindow.location.port
      );
    }
  }
}
